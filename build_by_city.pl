#!/usr/bin/perl

open(IN,"data.csv");
open(O,">city.csv");
open(P,">precinct.csv");
my %city;
my %precinct;
my %tot;

while(<IN>) {
  chomp;
  my @data = split(/,/,$_);
  # 0000023 AN0 16
  my $cty = substr($data[0],8,2);
  $precinct{$cty} .= "$data[0], ";
  $city{$cty}{'yes'} += $data[4];
  $city{$cty}{'no'}  += $data[7];
  $city{$cty}{'tot'} += $data[8];
  $tot{'yes'} += $data[4];
  $tot{'no'}  += $data[7];
  $tot{'tot'} += $data[8];

}
close(IN);

print P "CITY PRECINCT LIST\n";
print P "CITY,PRECINCTS\n";
foreach(keys %precinct) {
  print P "$_,$precinct{$_}\n";
}
close(P);

print O "2012 ConFire Measure Q\n";
print O "\n";
print O "CITY,YES,NO,TOTAL,PERCENT YES\n";

foreach my $cty (keys %city) {
   my $percent = "0%";
  if($city{$cty}{'yes'} > 0) {
    $percent = sprintf("%.02f\%",  ($city{$cty}{'yes'} / $city{$cty}{'tot'}) * 100);
  }
  print O "$cty,$city{$cty}{'yes'},$city{$cty}{'no'},$city{$cty}{'tot'},$percent\n";
}
my $percent = sprintf("%.02f\%",  ($tot{'yes'} / $tot{'tot'}) * 100);
print O "TOTAL,$tot{'yes'},$tot{'no'},$tot{'tot'},$percent\n";
close(O);
